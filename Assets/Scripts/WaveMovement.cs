﻿using UnityEngine;

public class WaveMovement : MonoBehaviour
{
	[SerializeField] private AnimationCurve animCurve;
	
	
	 private float animSpeed;
	 private Vector3 direction;
	 private Vector3 startPosition;
	
	 private Transform trans;
	
	 private void Awake()
	 {
	 	direction = direction.normalized;
	
	 	trans = transform;
	
	 	startPosition = trans.position;
	
	 	animSpeed = Random.Range(0.5f, 1.5f);
	
	 	direction = Random.Range(0, 3) switch
	 	{
	 		0 => trans.forward,
	 		1 => trans.up,
	 		2 => trans.right,
	 		_ => Vector3.zero
	 	};
	 }
	
	 private void Update()
	 {
	 	float t = (Time.time * animSpeed) % 1.0f;
	 	float displacement = animCurve.Evaluate(t);
	
		Vector3 newPosition = startPosition + direction * displacement;
	 	trans.position = newPosition;
	 }
}
